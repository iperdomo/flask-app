#!/usr/bin/env bash

set -eu

echo "Sleeping..."
sleep 30

python -m flask --app sebn run --host=0.0.0.0