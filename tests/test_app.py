import pytest
import sebn
from pathlib import Path

resources = Path(__file__).parent / "resources"


@pytest.fixture()
def app():
    return sebn.app


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()


def test_dummy():
    assert 1 == 1
    assert None is not True


def test_index(client):
    response = client.get("/")
    assert b"<title>SEBN</title>" in response.data


def test_upload(client):
    response = client.post(
        "/upload", data={"upload": (resources / "file.txt").open("rb")}
    )
    assert response.headers["Location"] == "/"
    assert response.status_code == 302
