from flask import Flask, render_template, redirect, url_for
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from werkzeug.utils import secure_filename
import os

one_million = 10**6


class LeakyClass:
    def __init__(self):
        self.data = [0] * (one_million)  # Allocate a large list


leaky_list = []


def create_memory_leak():
    for _ in range(1000):
        leaky_list.append(LeakyClass())  # Keep adding instances to the list


def is_prime(n):
    """Check if a number is prime."""
    if n <= 1:
        return False
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False
    return True


def compute_primes(limit):
    """Compute all prime numbers up to a given limit."""
    primes = []
    for num in range(2, limit):
        if is_prime(num):
            primes.append(num)
    return primes


def high_cpu():
    compute_primes(one_million * 10)


class UploadForm(FlaskForm):
    upload = FileField(validators=[FileRequired()])


app = Flask(__name__, instance_path="/tmp")


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/upload", methods=["GET", "POST"])
def upload():
    form = UploadForm(meta={"csrf": False})

    if form.validate_on_submit():
        f = form.upload.data
        filename = secure_filename(f.filename)
        f.save(os.path.join(app.instance_path, filename))
        # print(f)
        return redirect(url_for("index"))

    return render_template("upload.html", form=form)


@app.route("/leak", methods=["GET"])
def leak():
    create_memory_leak()
    return "OK"


@app.route("/cpu", methods=["GET"])
def cpu():
    high_cpu()
    return "OK"
